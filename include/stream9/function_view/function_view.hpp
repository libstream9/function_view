#ifndef STREAM9_FUNCTION_VIEW_FUNCTION_VIEW_HPP
#define STREAM9_FUNCTION_VIEW_FUNCTION_VIEW_HPP

#include <concepts>
#include <memory>
#include <type_traits>
#include <utility>

namespace stream9 {

template<typename Signature> class function_view;

template<typename R, typename... Args>
class function_view<R(Args...)>
{
public:
    function_view() = default;

    template<typename T>
        requires std::is_invocable_r_v<R, T, Args...>
              && (!std::same_as<std::remove_cvref_t<T>, function_view>)
    function_view(T&& func) noexcept
        : m_ptr { reinterpret_cast<void*>(std::addressof(func)) }
    {
        m_func = [](void* ptr, Args... args) -> R {
            auto& func = *reinterpret_cast<std::add_pointer_t<T>>(ptr);
            return func(std::forward<Args>(args)...);
        };
    }

    function_view(function_view const&) = default;
    function_view& operator=(function_view const&) = default;

    function_view(function_view&&) = default;
    function_view& operator=(function_view&&) = default;

    ~function_view() = default;

    explicit operator bool() const noexcept { return m_ptr; }

    R operator()(Args... args) const
        noexcept(noexcept(m_func(m_ptr, std::forward<Args>(args)...)))
    {
        return m_func(m_ptr, std::forward<Args>(args)...);
    }

private:
    void* m_ptr = nullptr;
    R (*m_func)(void*, Args...) = nullptr;
};

namespace detail {

    template<typename Functor> struct function_view_signature;

    template<typename R, typename F, bool Nx, typename... Args>
    struct function_view_signature<R(F::*)(Args...) noexcept(Nx)>
    {
        using type = R(Args...);
    };

    template<typename R, typename F, bool Nx, typename... Args>
    struct function_view_signature<R(F::*)(Args...) const noexcept(Nx)>
    {
        using type = R(Args...);
    };

    template<typename T>
    using function_view_signature_t = function_view_signature<T>::type;

} // namespace detail

template<typename R, typename... Args>
function_view(R(*)(Args...)) -> function_view<R(Args...)>;

template<typename T>
function_view(T) -> function_view<
    detail::function_view_signature_t<decltype(&T::operator())> >;

} // namespace stream9

#endif // STREAM9_FUNCTION_VIEW_FUNCTION_VIEW_HPP
